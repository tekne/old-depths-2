use bevy::prelude::*;
use bevy_prototype_character_controller::rapier::RapierDynamicImpulseCharacterControllerPlugin;
use bevy_rapier3d::physics::RapierPhysicsPlugin;

use depths::app_state::*;
use depths::voxel::world::VoxelWorldPlugin;

pub const WINDOW_WIDTH: u64 = 1920;
pub const WINDOW_HEIGHT: u64 = 1028;

fn main() {
    App::build()
        .insert_resource(WindowDescriptor {
            title: "depths".to_string(),
            width: WINDOW_WIDTH as f32,
            height: WINDOW_HEIGHT as f32,
            ..Default::default()
        })
        .insert_resource(ClearColor(Color::rgb_u8(0, 0, 0)))
        .add_plugins(DefaultPlugins)
        // State
        .insert_resource(State::new(AppState::Loading))
        .add_state(AppState::Loading)
        // Physics
        .add_plugin(RapierPhysicsPlugin)
        // Character controller
        .add_plugin(RapierDynamicImpulseCharacterControllerPlugin)
        // Terrain
        .add_plugin(VoxelWorldPlugin)
        .run()
}
