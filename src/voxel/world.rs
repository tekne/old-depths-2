/*!
A world of voxels; i.e. a large voxel map with level-of-detail support.
*/
use super::*;

/// A plugin which adds a voxel world to an `App`
pub struct VoxelWorldPlugin;

impl Plugin for VoxelWorldPlugin {
    fn build(&self, _app: &mut AppBuilder) {
        //TODO: this
        eprintln!("building VoxelWorldPlugin")
    }
}

/// A voxel world
pub struct VoxelWorld {
    /// An LOD map of voxels
    pub pyramid: ChunkHashMapPyramid3<Voxel>,
    /// An index for clipmap implementation
    pub index: OctreeChunkIndex,
}
