/*!
Voxels and their properties
*/

use bevy::prelude::*;
use building_blocks::mesh::{IsOpaque, MergeVoxel};
use building_blocks::prelude::*;

pub mod world;
/// A voxel. Carries a block ID extended with flags
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd, Default)]
pub struct Voxel(u64);

impl Voxel {
    /// The flag for a block ID being opaque
    pub const OPAQUE_FLAG: u64 = 1 << 63;
    /// A mask which excludes all flags
    pub const FLAG_MASK: u64 = Self::OPAQUE_FLAG;
    /// The voxel for a solid test block
    pub const TEST_SOLID: Voxel = Voxel(1 | Self::OPAQUE_FLAG);
}

impl IsEmpty for Voxel {
    fn is_empty(&self) -> bool {
        self.0 == 0
    }
}

impl IsOpaque for Voxel {
    fn is_opaque(&self) -> bool {
        self.0 & Self::OPAQUE_FLAG != 0
    }
}

impl MergeVoxel for Voxel {
    type VoxelValue = u64;
    fn voxel_merge_value(&self) -> Self::VoxelValue {
        self.0
    }
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn default_voxel_properties() {
        let e = Voxel::default();
        assert!(e.is_empty());
        assert!(!e.is_opaque());
        assert_eq!(e.voxel_merge_value(), 0);
    }
}
