/*!
Application state
*/

/// The current application state
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub enum AppState {
    /// The game is loading
    Loading,
    /// The game is running
    Running,
}
